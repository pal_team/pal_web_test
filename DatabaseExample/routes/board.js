var express = require('express');
var router = express.Router();




// 처음 localhost:3000으로 접속시
var index= function(req, res) {
	
	res.write('<a href="/board/list/1"> 글목록 </a>');
	
};

// 글 목록으로 갔을시
var list = function(req,res){
	var pool = req.app.get('database');
    pool.getConnection(function (err, connection) {
    	
    	// 넘어온 page 숫자에 따라 시작점을 만들어 줌
    	var str = (req.params.page-1)*5;
    	// 보여줄 페이지를 넣어줄 변수
    	var b;
    	// 저장되어있는 글개수를 넣어줄 변수
    	var c;
    	// 보여줄 페이지  sql문
        var sqlForSelectList = "SELECT no,name,content ,DATE_FORMAT(date, '%Y-%m-%d') date FROM board LIMIT "+str+",5 ;";
     // 저장되어있는 글개수 sql문
        var  a= "SELECT max(@a := @a + 1) AS rownum FROM(SELECT * FROM board  ) a,  ( SELECT @a := 0 ) b ; ";
        
        connection.query(sqlForSelectList, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            b=rows;
        });
        connection.query(a, function (err, rows) {
            if (err) console.error("err : " + err);
            console.log("rows : " + JSON.stringify(rows));
            c=rows;
            // board.ejs로 db에서 가져온 값들을 보내준다.
            connection.release();
            res.render('board', {title: ' 게시판 전체 글 조회', c:c,b:b});
        });
        
        
    });
};

// 글쓰기를 눌렀을시 html로 바로 보내줌
var insertForm = function(req,res){
    res.redirect("/public/boardForm.html");
};

// 글작성후 전송을 눌렀을때
var insert = function(req,res){
	var pool = req.app.get('database');
	pool.getConnection(function(err, conn) {
        if (err) {
        	conn.release();  // 반드시 해제해야 합니다.
          return;
        }   

    	var name=req.body.name;
    	var content=req.body.content;
    	var date=req.body.date;

    	// 데이터를 객체로 만듭니다.
    	var data = {name:name, content:content, date:date};
    	var a= 'insert into board set ?';
        var exec = conn.query(a, data, function(err, result) {
        	conn.release(); 
        	console.log('실행 대상 SQL : ' + exec.sql);
        	if (err) {
        		console.log('SQL 실행 시 에러 발생함.');
        		console.dir(err);
        		return;
        	}
        	 res.redirect("/board/list/1");
        });
	});
	
};

// 선택한 글번호정보를 불러와 업데이트폼으로 이동
var updateForm = function(req,res){
	var pool = req.app.get('database');
	var no = req.params.no;
	 pool.getConnection(function (err, connection) {
	        var sqlForSelectList = "SELECT no,name ,DATE_FORMAT(date, '%Y-%m-%d') date  FROM board where no="+no;
	        connection.query(sqlForSelectList, function (err, rows) {
	            if (err) console.error("err : " + err);
	            console.log("rows : " + JSON.stringify(rows));
	            connection.release();
	            res.render('update', {title: '게시판 글 수정', rows: rows});
	        });
	    });
	
};

// 수정한 값으로 수정
var update =  function(req,res){
	var pool = req.app.get('database');
	 pool.getConnection(function (err, connection) {
	    	var content = req.body.content;
	    	var no = req.body.no;
	        var sqlForSelectList = "update board set content='"+content+"' where no = "+no;
	        console.log(sqlForSelectList);
	        connection.query(sqlForSelectList, function (err, rows) {
	            if (err) console.error("err : " + err);
	            console.log("rows : " + JSON.stringify(rows));
	            connection.release();
	            res.redirect("/board/list/1");
	        });
	        });
	
	 
	
};

// 해당 글을 삭제
var del = function(req,res){
	var pool = req.app.get('database');
	 pool.getConnection(function (err, connection) {
	    	var no = req.params.no;
	        var sqlForSelectList = "delete from board where no = "+no;
	        connection.query(sqlForSelectList, function (err, rows) {
	            if (err) console.error("err : " + err);
	            console.log("rows : " + JSON.stringify(rows));
	            connection.release();
	            res.redirect("/board/list/1");
	        });
	        });
};

module.exports.index = index;
module.exports.insert = insert;
module.exports.insertForm = insertForm;
module.exports.list = list;
module.exports.update = update;
module.exports.updateForm = updateForm;
module.exports.del = del;


