/*
 * database
 */
var mysql = require('mysql');

var database = {};

database.init = function(app, config) {
	console.log('init() 호출됨.');
	
	connect(app, config);
}

//데이터베이스에 연결하고 응답 객체의 속성으로 db 객체 추가
function connect(app, config) {
	console.log('connect() 호출됨.');

	// 데이터베이스 연결 : config의 설정 사용
	database = mysql.createPool(config.db_info);
	
	app.set('database', database);
}

// database 객체를 module.exports에 할당
module.exports = database;
