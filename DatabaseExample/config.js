/*
 * 설정
 */
module.exports = {
	server_port: 3000,
	db_info:{
	    connectionLimit : 10, 
	    host     : 'localhost',
	    user     : 'root',
	    password : 'beom',
	    database : 'test',
	    debug    :  false
	},
	route_info: [
	    {file:'./board', path:'/', method:'index', type:'get'}					// user.login 
	    ,{file:'./board', path:'/board/list/:page', method:'list', type:'get'}						// user.login 
	    ,{file:'./board', path:'/board/insertForm', method:'insertForm', type:'get'}						// user.login 
	    ,{file:'./board', path:'/board/insert', method:'insert', type:'post'}						// user.login 
	    ,{file:'./board', path:'/board/updateForm/:no', method:'updateForm', type:'get'}						// user.login 
	    ,{file:'./board', path:'/board/update', method:'update', type:'post'}						// user.login 
	    ,{file:'./board', path:'/board/delete/:no', method:'del', type:'get'}						// user.login 
	   ]
}