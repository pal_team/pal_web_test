var express = require('express')
  , http = require('http')
  , path = require('path');
var ejs = require('ejs');
var expressErrorHandler = require('express-error-handler');


//===== MySQL 데이터베이스를 사용할 수 있도록 하는 mysql 모듈 불러오기 =====//
var mysql = require('mysql');
var board = require('./routes/board');   
var bodyParser = require('body-parser');
//MySQL 로드


//서버 기본 설정
var config = require('./config');
var database = require('./config/database');
var route_loader = require('./routes/route_loader');

//===== Express 서버 객체 만들기 =====//
var app = express();
var router=express.Router();
app.use(bodyParser.urlencoded({extended: true}));
app.set('views',path.join(__dirname,'views'));
app.set('view engine', 'ejs');

//===== 서버 변수 설정 및 static으로 public 폴더 설정  =====//
app.set('port', process.env.PORT || 3000);
app.use('/public', express.static(path.join(__dirname, 'public')));



//라우팅 정보를 읽어들여 라우팅 설정
route_loader.init(app);


//===== 404 에러 페이지 처리 =====//
var errorHandler = expressErrorHandler({
 static: {
   '404': './public/404.html'
 }
});

app.use( expressErrorHandler.httpError(404) );
app.use( errorHandler );


//===== 서버 시작 =====//
http.createServer(app).listen(app.get('port'), function(){
  console.log('서버가 시작되었습니다. 포트 : ' + app.get('port'));
  database.init(app, config);
});
