/**
 * http://usejsdoc.org/
 */
var index = function(req, res) {
	
	// 홈 화면 - 로그인 링크
	console.log('/ 패스 요청됨.');

	console.dir(req.user);
		
	if (req.user == undefined) {
		res.render('index.ejs', {login_success:false});
	} else {
		res.render('index.ejs', {login_success:true});
	}
};

module.exports.index = index;