//===== winston log =====//
// 로그 남기기 : winston 모듈을 이용해 로그 남기기

var winston = require('winston');    // 로그 처리 모듈
var moment = require('moment');    // 시간 처리 모듈

function timeStampFormat() {
    return moment().format('YYYY-MM-DD HH:mm:ss.SSS ZZ'); // '2016-05-01 20:14:28.500 +0900'
};

var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.DailyRotateFile)({
            name: 'info-file',
            filename: './log/server.log',
            datePattern: '.yyyy-MM-dd',
            colorize: 'false',
            maxsize: 20000,
            level: 'debug',
            timestamp : timeStampFormat
        }),
        new (winston.transports.Console)({
            name: 'debug-console',
            colorize: 'true',
            level: 'debug',
            timestamp : timeStampFormat
        })
    ],
    exceptionHandlers: [
        new (winston.transports.DailyRotateFile)({
            name: 'exception-file',
            filename: './log/exception.log',
            datePattern: '.yyyy-MM-dd',
            colorize: 'false',
            maxsize: 20000,
            level: 'error',
            timestamp : timeStampFormat
        }),
        new (winston.transports.Console)({
            name: 'exception-console',
            colorize: 'true',
            level: 'error',
            timestamp : timeStampFormat
        })
    ]
});

module.exports = logger;
