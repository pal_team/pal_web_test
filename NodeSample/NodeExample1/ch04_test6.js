
/**
 * 4장 Test 13
 * 
 * FS 사용하기 : Non-Blocking IO
 */

var fs = require('fs');

//파일을 비동기식 IO 방식으로 읽어 들입니다.
fs.readFile('./README.md', 'utf8', function(err, data) {
	// 읽어 들인 데이터를 출력합니다.
	console.log(data);
});

console.log('프로젝트 폴더 안의 README.md 파일을 읽어들입니다.');
