
/**
 * 파일 업로드
 * 
 * 클라이언트에서 업로드 시 지정한 파일의 이름 : photo
 */

var express = require('express')
  , http = require('http')
  , path = require('path')
  , mime = require('mime');

var bodyParser = require('body-parser');
var cookieParser = require('cookie-parser');
var expressSession = require('express-session');

// 파일 업로드용 미들웨어
var multer = require('multer');
var fs = require('fs');


//클라이언트에서 ajax로 요청 시 CORS(다중 서버 접속) 지원
var cors = require('cors');


// 에러 핸들러 모듈 사용
var expressErrorHandler = require('express-error-handler');

var app = express();

app.set('port', process.env.PORT || 3000);

app.use('/public', express.static(path.join(__dirname, 'public')));
app.use('/uploads', express.static(path.join(__dirname, 'uploads')));

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());


/*
// TEST logging
app.use(function(req, res, next){

	console.log(JSON.stringify(req.headers));
	
    req.on('data', function(chunk){
        console.log("RAW data : " + chunk);
    });
    
    req.on('end', function(){
        next();
    });
    
})
*/


app.use(cookieParser());
app.use(expressSession({
	secret:'my key',
	resave:true,
	saveUninitialized:true
}));


//클라이언트에서 ajax로 요청 시 CORS(다중 서버 접속) 지원
app.use(cors());


// 파일 업로드용

//multer 미들웨어를 사용 : 미들웨어 사용 순서 중요  body-parser -> multer -> router
// 파일 제한 : 10개, 1G
app.use(multer({ 
	dest: 'uploads',
	putSingleFilesInArray: true,
	limits: {
		files: 10,
		fileSize: 1024 * 1024 * 1024
	},
	rename: function (fieldname, filename) {
	    return filename+Date.now();
	},
	onFileUploadStart: function (file, req, res) {
	  file.uploadData = 0;
	  console.log('파일 업로드 시작 : '+ file.originalname);
	  
	  var totalSize = req.param("totalSize");
	  console.log('전체 크기 : %s', totalSize);
	  
	  if (totalSize) {
		  file.totalSize = parseFloat(totalSize);
	  }
	},
	onFileUploadComplete: function (file, req, res) {
	  console.log('파일 업로드 완료 : ' + file.fieldname + ' ->  ' + file.path);
	},
	onFileUploadData: function(file, data) {
      file.uploadData = file.uploadData + data.length;
      
      if (file.totalSize) {
    	  var progressValue = Math.floor((file.uploadData / file.totalSize)*100.0);
    	  console.log("파일 업로드중 : " + file.uploadData + ", 진행률 : " + progressValue + "%");
      } else {
    	  console.log("파일 업로드중 : " + file.uploadData);
      }
    },
	onParseStart: function() {
        console.log('파싱 시작 : ', new Date())
    },
    onParseEnd: function (req, next) {
        console.log('파싱 완료 : ', new Date());
        next(); // 파싱이 끝나면 next() 함수를 반드시 호출해주어야 함
    },
    onFileSizeLimit: function (file) {
		console.log('파일 크기 제한 초과 : %s', file.originalname);
	},
    onFilesLimit: function () {
        console.log('파일 제한 초과.')
    },
    onFieldsLimit: function () {
        console.log('필드 제한 초과.')
    },
    onPartsLimit: function () {
        console.log('파트 제한 초과.')
    },
    onError: function (error, next) {
        console.log("업로드 중 에러 발생함.");
        next(); // 에러 발생 시, next() 함수를 반드시 호출해주어야 함
    }
}));

 

//파일 다운로드 패스에 대한 라우팅
app.post('/process/download', function(req, res) {
	console.log('/process/download 호출됨.');
	
	try {
		var paramFilepath = req.param('filepath');
		var filepath = __dirname + paramFilepath;
		var filename = path.basename(paramFilepath);
        var mimetype = mime.lookup(paramFilepath);
		
		console.log('파일 패스 : ' + filepath);
		console.log('파일 이름 : ' + filename);
		console.log('MIME 타입 : ' + mimetype);
		
		// 파일 크기 확인
		var stats = fs.statSync(filepath);
		var fileSize = stats["size"];
		console.log('파일 크기 : ' + fileSize);
		
		// 클라이언트에 응답 전송
		res.setHeader('Content-disposition', 'attachment; filename=' + filename);
	    res.setHeader('Content-type', mimetype);
	    res.setHeader('Content-Length', fileSize);
	  
	    var filestream = fs.createReadStream(filepath);
	    filestream.pipe(res);
	    
	} catch(err) {
		console.dir(err.stack);
		
		res.writeHead('400', {'Content-Type':'text/html;charset=utf8'});
		res.write('<h3>파일 업로드 실패</h3>');
		res.end();
	}	
		
});


// 파일 업로드 패스에 대한 라우팅
app.post('/process/photo', function(req, res) {
	console.log('/process/photo 호출됨.');
	
	try {
		var files = req.files.photo;
	
		// 현재의 파일 정보를 저장할 변수 선언
		var originalname = '',
			name = '',
			mimetype = '',
			size = 0;
		
		if (Array.isArray(files)) {   // 배열에 들어가 있는 경우 (설정에서 1개의 파일도 배열에 넣게 했음)
	        console.log("배열에 들어있는 파일 갯수 : %d", files.length);
	        
	        for (var index = 0; index < files.length; index++) {
	        	originalname = files[index].originalname;
	        	name = files[index].name;
	        	mimetype = files[index].mimetype;
	        	size = files[index].size;
	        }
	        
	    } else {   // 배열에 들어가 있지 않은 경우 (현재 설정에서는 해당 없음)
	        console.log("파일 갯수 : 1 ");
	        
	    	originalname = files[index].originalname;
	    	name = files[index].name;
	    	mimetype = files[index].mimetype;
	    	size = files[index].size;
	    }
		
		console.log('현재 파일 정보 : ' + originalname + ', ' + name + ', '
				+ mimetype + ', ' + size);
		
		// 클라이언트에 응답 전송
		res.writeHead('200', {'Content-Type':'text/html;charset=utf8'});
		res.write('<h3>파일 업로드 성공</h3>');
		res.write('<hr/>');
		res.write('<p>원본 파일명 : ' + originalname + ' -> 저장 파일명 : ' + name + '</p>');
		res.write('<p>MIME TYPE : ' + mimetype + '</p>');
		res.write('<p>파일 크기 : ' + size + '</p>');
		res.end();
		
	} catch(err) {
		console.dir(err.stack);
	}	
		
});


app.get('/process/product', function(req, res) {
	console.log('/process/product 호출됨.');
	
	if (req.session.user) {
		res.redirect('/public/product.html');
	} else {
		res.redirect('/public/login2.html');
	}
});

app.post('/process/login', function(req, res) {
	console.log('/process/login 호출됨.');

	var paramId = req.param('id');
	var paramPassword = req.param('password');
	
	if (req.session.user) {
		// 이미 로그인된 상태
		console.log('이미 로그인되어 상품 페이지로 이동합니다.');
		
		res.redirect('/public/product.html');
	} else {
		// 세션 저장
		req.session.user = {
			id: paramId,
			name: '소녀시대',
			authorized: true
		};
		
		res.writeHead('200', {'Content-Type':'text/html;charset=utf8'});
		res.write('<h1>로그인 성공</h1>');
		res.write('<div><p>Param id : ' + paramId + '</p></div>');
		res.write('<div><p>Param password : ' + paramPassword + '</p></div>');
		res.write("<br><br><a href='/process/product'>상품 페이지로 이동하기</a>");
		res.end();
	}
});

app.get('/process/logout', function(req, res) {
	console.log('/process/login 호출됨.');
	
	if (req.session.user) {
		// 로그인된 상태
		console.log('로그아웃합니다.');
		
		req.session.destroy(function(err) {
			if (err) {throw err;}
			
			console.log('세션을 삭제하고 로그아웃되었습니다.');
			res.redirect('/public/login2.html');
		});
	} else {
		// 로그인 안된 상태
		console.log('아직 로그인되어있지 않습니다.');
		
		res.redirect('/public/login2.html');
	}
});	

// 404 에러 페이지 처리
var errorHandler = expressErrorHandler({
    static: {
      '404': './public/404.html'
    }
});

app.use( expressErrorHandler.httpError(404) );
app.use( errorHandler );


var server = http.createServer(app);
server.listen(app.get('port'), function(){
  console.log('Express server listening on %s, %s', server.address().address, server.address().port);
});
