
/**
 * Redis를 이용하는 Chat 프로젝트
 */

//===== 모듈 불러들이기 =====//
var express = require('express')
  , http = require('http')
  , path = require('path');
 
 
//===== Socket.IO 사용 =====//
var socketio = require('socket.io');


//===== redis 모듈 불러온 후 레디스 클라이언트 객체 만들기 =====//
var redis = require("redis");
var pub = redis.createClient(6379,'127.0.0.1');
var sub = redis.createClient(6379,'127.0.0.1');
var store = redis.createClient(6379,'127.0.0.1');
 

//===== Express 서버 객체 만들기 =====//
var app = express();

 
//===== 404 에러 페이지 처리 =====//
var errorHandler = expressErrorHandler({
 static: {
   '404': './public/404.html'
 }
});

app.use( expressErrorHandler.httpError(404) );
app.use( errorHandler );


//===== 서버 시작 =====//

// 시작된 서버 객체를 리턴받도록 합니다. 
var server = http.createServer(app).listen(3000, function(){
	console.log('서버가 시작되었습니다. 포트 : ' + app.get('port'));
});


//socket.io 서버를 시작합니다.
var io = socketio.listen(server);
console.log('socket.io 요청을 받아들일 준비가 되었습니다.');

io.set('store',new socketio.RedisStore({
	redis: redis
    ,redisPub: pub
    ,redisSub: sub
    ,redisClient: store
}));


//클라이언트가 연결했을 때의 이벤트 처리
io.sockets.on('connection', function(socket) {
	console.log('connection info :', socket.request.connection._peername);

	// 소켓 객체에 클라이언트 Host, Port 정보 속성으로 추가
	socket.remoteAddress = socket.request.connection._peername.address;
	socket.remotePort = socket.request.connection._peername.port;
});


