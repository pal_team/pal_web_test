
/**
 * router 미들웨어 추가하기
 */

var express = require('express')
  , http = require('http')
  , path = require('path');

var bodyParser = require('body-parser');


//Redis 사용하기
var redis = require("redis");
var store = redis.createClient(); 
var pub = redis.createClient();
var sub = redis.createClient();


var channel_name = 'chat';
sub.on('message', function(channel, dataStr) {
	console.log('Redis subscriber received message on channel ' + channel);

	// process JSON formatted string input
	var data = JSON.parse(dataStr);
	console.log('DATA : %j', data);
});

sub.subscribe(channel_name);
console.log('redis에 subscribe하였습니다. : ' + channel_name);





var app = express();

app.set('port', process.env.PORT || 3000);

app.use('/public', express.static(path.join(__dirname, 'public')));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
 

app.post('/process/publish', function(req, res) {
	console.log('/process/publish 처리함.');

	var paramSender = req.body.sender;
	var paramReceiver = req.body.receiver;
	var paramContents = req.body.contents;
	
	var data = {sender:paramSender, receiver:paramReceiver, contents:paramContents};
	var dataStr = JSON.stringify(data);
	pub.publish('chat', dataStr);
	
	res.writeHead('200', {'Content-Type':'text/html;charset=utf8'});
	res.write('<h1>서버에서 응답한 결과입니다.</h1>');
	res.write('<div><p>redis로 publish했습니다. 서버 로그를 확인하세요. : ' + dataStr + '</p></div>');
	res.write("<br><br><a href='/public/publish.html'>처음으로 돌아가기</a>");
	res.end();
});


app.all('*', function(req, res) {
	res.send(404, '<h1>ERROR - 페이지를 찾을 수 없습니다.</h1>')
});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
